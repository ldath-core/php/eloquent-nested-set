<?php

return [
    ['id' => 1, 'name' => 'store', 'lft' => 1, 'rgt' => 20, 'lvl' => 0],
        ['id' => 2, 'name' => 'notebooks', 'lft' => 2, 'rgt' => 7, 'lvl' => 1],
            ['id' => 3, 'name' => 'apple', 'lft' => 3, 'rgt' => 4, 'lvl' => 2],
            ['id' => 4, 'name' => 'lenovo', 'lft' => 5, 'rgt' => 6, 'lvl' => 2],
        ['id' => 5, 'name' => 'mobile', 'lft' => 8, 'rgt' => 19, 'lvl' => 1],
            ['id' => 6, 'name' => 'nokia', 'lft' => 9, 'rgt' => 10, 'lvl' => 2],
            ['id' => 7, 'name' => 'samsung', 'lft' => 11, 'rgt' => 14, 'lvl' => 2],
                ['id' => 8, 'name' => 'galaxy', 'lft' => 12, 'rgt' => 13, 'lvl' => 3],
            ['id' => 9, 'name' => 'sony', 'lft' => 15, 'rgt' => 16, 'lvl' => 2],
            ['id' => 10, 'name' => 'lenovo', 'lft' => 17, 'rgt' => 18, 'lvl' => 5],
//    ['id' => 11, 'name' => 'store_2', 'lft' => 21, 'rgt' => 22, 'lvl' => 0],
];