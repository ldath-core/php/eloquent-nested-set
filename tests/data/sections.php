<?php

return [
    ['id' => 1, 'title' => 'Home', 'tree_left' => 1, 'tree_right' => 8, 'tree_level' => 0],
        ['id' => 2, 'title' => 'World', 'tree_left' => 2, 'tree_right' => 5, 'tree_level' => 1],
            ['id' => 3, 'title' => 'Europe', 'tree_left' => 3, 'tree_right' => 4, 'tree_level' => 2],
        ['id' => 4, 'title' => 'Business', 'tree_left' => 6, 'tree_right' => 7, 'tree_level' => 1]
];