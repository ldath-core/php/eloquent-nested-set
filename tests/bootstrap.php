<?php

use Illuminate\Database\Capsule\Manager;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;
use Thunderwolf\EloquentNestedSet\NestedSetServiceProvider;

include __DIR__.'/../vendor/autoload.php';

$container = new Container();
$provider = new NestedSetServiceProvider($container);
$provider->register();

$capsule = new Manager($container);
$capsule->addConnection([ 'driver' => 'sqlite', 'database' => ':memory:', 'prefix' => 'prfx_' ]);
$capsule->setEventDispatcher(new Dispatcher);
$capsule->setAsGlobal();
$capsule->bootEloquent();

include __DIR__.'/models/Category.php';
include __DIR__.'/models/Section.php';
include __DIR__.'/models/PostsThread.php';
include __DIR__.'/models/Post.php';
