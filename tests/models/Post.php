<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Thunderwolf\EloquentNestedSet\NestedSet;

class Post extends Model
{
    use NestedSet;

    protected $table = 'posts';

    protected $fillable = ['code', 'body'];

    public $timestamps = false;

    public static function nestedSet(): array
    {
        return ['use_scope' => true, 'scope_column' => 'posts_thread_id'];
    }

    public static function resetActionsPerformed()
    {
        static::$actionsPerformed = 0;
    }

    public function thread(): BelongsTo
    {
        return $this->belongsTo(PostsThread::class, 'posts_thread_id');
    }
}