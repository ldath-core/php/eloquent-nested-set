<?php

use Illuminate\Database\Eloquent\Model;
use Thunderwolf\EloquentNestedSet\NestedSet;

class Category extends Model
{

    use NestedSet;

    protected $table = 'categories';

    protected $fillable = ['name'];

    public $timestamps = false;

    public static function nestedSet(): array
    {
        return ['left' => 'lft', 'right' => 'rgt', 'level' => 'lvl'];
    }

    public static function resetActionsPerformed()
    {
        static::$actionsPerformed = 0;
    }
}