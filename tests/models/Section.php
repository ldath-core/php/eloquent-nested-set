<?php

use Illuminate\Database\Eloquent\Model;
use Thunderwolf\EloquentNestedSet\NestedSet;

class Section extends Model
{
    use NestedSet;

    protected $table = 'sections';

    protected $fillable = ['title'];

    public $timestamps = false;

    public static function nestedSet(): array
    {
        return [];
    }

    public static function resetActionsPerformed()
    {
        static::$actionsPerformed = 0;
    }
}