<?php

use Illuminate\Database\Capsule\Manager;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Schema\Blueprint;
use PHPUnit\Framework\TestCase;

class NestedSetTest extends TestCase
{
    public static function setUpBeforeClass(): void
    {
        $schema = Manager::schema();

        $schema->dropIfExists('categories');
        $schema->dropIfExists('sections');
        $schema->dropIfExists('posts');
        $schema->dropIfExists('posts-threads');

        Manager::connection()->disableQueryLog();

        $schema->create('categories', function (Blueprint $table1) {
            $table1->increments('id');
            $table1->string('name');
            $table1->createNestedSet(['left' => 'lft', 'right' => 'rgt', 'level' => 'lvl']);
        });

        $schema->create('sections', function (Blueprint $table2) {
            $table2->increments('id');
            $table2->string('title');
            $table2->createNestedSet([]);
        });

        $schema->create('posts-threads', function (Blueprint $table3) {
            $table3->increments('id');
            $table3->string('title');
        });

        $schema->create('posts', function (Blueprint $table4) {
            $table4->increments('id');
            $table4->unsignedInteger('posts_thread_id');
            $table4->string('code');
            $table4->string('body');
            $table4->createNestedSet(['use_scope' => true, 'scope_column' => 'posts_thread_id']);
        });

        Manager::connection()->enableQueryLog();
    }

    public function setUp(): void
    {
        $categories_data = include __DIR__.'/data/categories.php';

        Manager::table('categories')->insert($categories_data);

        $sections_data = include __DIR__.'/data/sections.php';

        Manager::table('sections')->insert($sections_data);

        $threads_data = include __DIR__.'/data/threads.php';
        $posts_data = include __DIR__.'/data/posts.php';
        Manager::table('posts-threads')->insert($threads_data);
        Manager::table('posts')->insert($posts_data);

        Manager::connection()->flushQueryLog();

        Category::resetActionsPerformed();
        Section::resetActionsPerformed();

        date_default_timezone_set('Europe/Warsaw');
    }

    public function tearDown(): void
    {
        Manager::table('categories')->truncate();
        Manager::table('sections')->truncate();
        Manager::table('posts')->truncate();
        Manager::table('posts-threads')->truncate();
    }

    public function testRecordOne() {
        $article = Category::query()->find(1);

        $this->assertEquals('store', $article->getAttribute('name'));
    }

    public function testCreateSectionTree() {
        Manager::table('sections')->truncate();
        $s1 = new Section();
        $s1->setAttribute('title', 'Home');
        $s1->makeRoot(); // make this node the root of the tree
        $s1->save();

        $this->assertEquals(1, $s1->getAttribute('tree_left'));
        $this->assertEquals(2, $s1->getAttribute('tree_right'));
        $this->assertEquals(0, $s1->getAttribute('tree_level'));

        $s2 = new Section();
        $s2->setAttribute('title', 'World');
        $s2->insertAsFirstChildOf($s1); // insert the node in the tree
        $s2->save();

        $this->assertEquals(2, $s2->getAttribute('tree_left'));
        $this->assertEquals(3, $s2->getAttribute('tree_right'));
        $this->assertEquals(1, $s2->getAttribute('tree_level'));

        $s1 = Section::find($s1->getKey());

        $this->assertEquals(1, $s1->getAttribute('tree_left'));
        $this->assertEquals(4, $s1->getAttribute('tree_right'));
        $this->assertEquals(0, $s1->getAttribute('tree_level'));

        $s3 = new Section();
        $s3->setAttribute('title', 'Europe');
        $s3->insertAsFirstChildOf($s2); // insert the node in the tree
        $s3->save();

        $this->assertEquals(3, $s3->getAttribute('tree_left'));
        $this->assertEquals(4, $s3->getAttribute('tree_right'));
        $this->assertEquals(2, $s3->getAttribute('tree_level'));

        $s1 = Section::find($s1->getKey());
        $s2 = Section::find($s2->getKey());

        $this->assertEquals(1, $s1->getAttribute('tree_left'));
        $this->assertEquals(6, $s1->getAttribute('tree_right'));
        $this->assertEquals(0, $s1->getAttribute('tree_level'));

        $this->assertEquals(2, $s2->getAttribute('tree_left'));
        $this->assertEquals(5, $s2->getAttribute('tree_right'));
        $this->assertEquals(1, $s2->getAttribute('tree_level'));

        $s4 = new Section();
        $s4->setAttribute('title', 'Business');
        $s4->insertAsNextSiblingOf($s2); // insert the node in the tree
        $s4->save();

        $this->assertEquals(6, $s4->getAttribute('tree_left'));
        $this->assertEquals(7, $s4->getAttribute('tree_right'));
        $this->assertEquals(1, $s4->getAttribute('tree_level'));

        $s1 = Section::find($s1->getKey());
        $s2 = Section::find($s2->getKey());
        $s3 = Section::find($s3->getKey());

        $this->assertEquals(1, $s1->getAttribute('tree_left'));
        $this->assertEquals(8, $s1->getAttribute('tree_right'));
        $this->assertEquals(0, $s1->getAttribute('tree_level'));

        $this->assertEquals(2, $s2->getAttribute('tree_left'));
        $this->assertEquals(5, $s2->getAttribute('tree_right'));
        $this->assertEquals(1, $s2->getAttribute('tree_level'));

        $this->assertEquals(3, $s3->getAttribute('tree_left'));
        $this->assertEquals(4, $s3->getAttribute('tree_right'));
        $this->assertEquals(2, $s3->getAttribute('tree_level'));

        /* The sections are now stored in the database as a tree:
            $s1:Home
            |       \
        $s2:World  $s4:Business
            |
        $s3:Europe
        */
    }

    public function testAddChildAndRefresh()
    {
        $rootNode = Section::query()->findRoot();
        $this->assertEquals('Home', $rootNode->getAttribute('title'));
        $this->assertEquals(1, $rootNode->getAttribute('tree_left'));
        $this->assertEquals(8, $rootNode->getAttribute('tree_right'));
        $this->assertEquals(0, $rootNode->getAttribute('tree_level'));

        $worldNode = $rootNode->getFirstChild();        // $s2
        $this->assertEquals('World', $worldNode->getAttribute('title'));
        $this->assertEquals(2, $worldNode->getAttribute('tree_left'));
        $this->assertEquals(5, $worldNode->getAttribute('tree_right'));
        $this->assertEquals(1, $worldNode->getAttribute('tree_level'));

        $child = new Section();
        $child->setAttribute('title', 'New Child');
        $rootNode->addChild($child);
        $this->assertEquals('Home', $rootNode->getAttribute('title'));
        $this->assertEquals(1, $rootNode->getAttribute('tree_left'));
        $this->assertEquals(10, $rootNode->getAttribute('tree_right'));
        $this->assertEquals(0, $rootNode->getAttribute('tree_level'));

        $worldNode->refresh();
        $this->assertEquals('World', $worldNode->getAttribute('title'));
        $this->assertEquals(4, $worldNode->getAttribute('tree_left'));
        $this->assertEquals(7, $worldNode->getAttribute('tree_right'));
        $this->assertEquals(1, $worldNode->getAttribute('tree_level'));
    }

    public function testBasicTreeMethods() {
        $rootNode = Section::query()->findRoot();
        $this->assertEquals('Home', $rootNode->getAttribute('title'));
        $this->assertEquals(1, $rootNode->getAttribute('tree_left'));
        $this->assertEquals(8, $rootNode->getAttribute('tree_right'));
        $this->assertEquals(0, $rootNode->getAttribute('tree_level'));

        $worldNode = $rootNode->getFirstChild();        // $s2
        $this->assertEquals('World', $worldNode->getAttribute('title'));
        $this->assertEquals(2, $worldNode->getAttribute('tree_left'));
        $this->assertEquals(5, $worldNode->getAttribute('tree_right'));
        $this->assertEquals(1, $worldNode->getAttribute('tree_level'));

        $businessNode = $worldNode->getNextSibling();   // $s4
        $this->assertEquals('Business', $businessNode->getAttribute('title'));
        $this->assertEquals(6, $businessNode->getAttribute('tree_left'));
        $this->assertEquals(7, $businessNode->getAttribute('tree_right'));
        $this->assertEquals(1, $businessNode->getAttribute('tree_level'));

        // you can also chain the methods
        $europeNode = $rootNode->getLastChild()->getPrevSibling()->getFirstChild();  // $s3
        $this->assertEquals('Europe', $europeNode->getAttribute('title'));
        $this->assertEquals(3, $europeNode->getAttribute('tree_left'));
        $this->assertEquals(4, $europeNode->getAttribute('tree_right'));
        $this->assertEquals(2, $europeNode->getAttribute('tree_level'));

        $firstLevelSections = $rootNode->getChildren(); // array($s2, $s4)
        $this->assertContainsOnlyInstancesOf(
            Section::class,
            $firstLevelSections
        );
        $this->assertInstanceOf(Collection::class, $firstLevelSections);
        $this->assertEquals('World', $firstLevelSections->find($worldNode->getKey())->getAttribute('title'));
        $this->assertEquals('Business', $firstLevelSections->find($businessNode->getKey())->getAttribute('title'));


        $allSections = $rootNode->getDescendants();     // array($s2, $s3, $s4)
        $this->assertContainsOnlyInstancesOf(
            Section::class,
            $allSections
        );
        $this->assertInstanceOf(Collection::class, $allSections);
        $this->assertEquals('World', $allSections->find($worldNode->getKey())->getAttribute('title'));
        $this->assertEquals('Europe', $allSections->find($europeNode->getKey())->getAttribute('title'));
        $this->assertEquals('Business', $allSections->find($businessNode->getKey())->getAttribute('title'));

        $path = $europeNode->getAncestors();            // array($s1, $s2)
        $this->assertContainsOnlyInstancesOf(
            Section::class,
            $path
        );
        $this->assertInstanceOf(Collection::class, $path);
        $this->assertEquals('Home', $path->find($rootNode->getKey())->getAttribute('title'));
        $this->assertEquals('World', $path->find($worldNode->getKey())->getAttribute('title'));
    }

    public function testInspectionMethods() {
        $rootNode = Section::query()->findRoot();
        $worldNode = $rootNode->getFirstChild();

        $this->assertIsBool($worldNode->isRoot());
        $this->assertEquals(false, $worldNode->isRoot());

        $this->assertIsBool($worldNode->isLeaf());
        $this->assertEquals(false, $worldNode->isLeaf());

        $this->assertIsInt($worldNode->getLvl());
        $this->assertEquals(1, $worldNode->getLvl());

        $this->assertIsBool($worldNode->hasChildren());
        $this->assertEquals(true, $worldNode->hasChildren());

        $this->assertIsInt($worldNode->countChildren());
        $this->assertEquals(1, $worldNode->countChildren());

        // TODO: looks like hasSiblings is missing
//        $this->assertIsBool($worldNode->hasSiblings());
//        $this->assertEquals(true, $worldNode->hasSiblings());
    }

    /**
     * @depends testBasicTreeMethods
     */
    public function testMoveAndDelete()
    {
        $rootNode = Section::query()->findRoot();
        $worldNode = $rootNode->getFirstChild();        // s2  2 - 5 - 1
        $businessNode = $worldNode->getNextSibling();   // s4  6 - 7 - 1

        /* The sections are now stored in the database as a tree:
            $s1:Home
            |       \
        $s2:World  $s4:Business
            |
        $s3:Europe
        */
        // move the entire "World" section under "Business"
        $s2 = $worldNode->moveToFirstChildOf($businessNode);
        /* The tree is modified as follows:
        $s1:Home
          |
        $s4:Business
          |
        $s2:World
          |
        $s3:Europe
        */
        $this->assertEquals(3, $s2->getAttribute('tree_left'));
        $this->assertEquals(6, $s2->getAttribute('tree_right'));
        $this->assertEquals(2, $s2->getAttribute('tree_level'));

        $s4 = Section::query()->find(4);
        $this->assertEquals(2, $s4->getAttribute('tree_left'));
        $this->assertEquals(7, $s4->getAttribute('tree_right'));
        $this->assertEquals(1, $s4->getAttribute('tree_level'));

        $s3 = Section::query()->find(3);
        $this->assertEquals(4, $s3->getAttribute('tree_left'));
        $this->assertEquals(5, $s3->getAttribute('tree_right'));
        $this->assertEquals(3, $s3->getAttribute('tree_level'));

        // now move the "Europe" section directly under root, after "Business"
        $s3 = $s3->moveToNextSiblingOf($s4);
        /* The tree is modified as follows:
            $s1:Home
            |        \
        $s4:Business $s3:Europe
            |
        $s2:World
        */
        $this->assertEquals(6, $s3->getAttribute('tree_left'));
        $this->assertEquals(7, $s3->getAttribute('tree_right'));
        $this->assertEquals(1, $s3->getAttribute('tree_level'));

        $s4 = Section::query()->find(4);

        // delete the entire "World" section of "Business"
        $deleted = $s4->deleteDescendants();
        /* The tree is modified as follows:
            $s1:Home
            |        \
        $s4:Business $s3:Europe
        */

        $this->assertEquals(1, $deleted);
        $s1 = Section::query()->find(1);
        $this->assertEquals(1, $s1->getAttribute('tree_left'));
        $this->assertEquals(6, $s1->getAttribute('tree_right'));
        $this->assertEquals(0, $s1->getAttribute('tree_level'));

        $s4 = Section::query()->find(4);
        $this->assertEquals(2, $s4->getAttribute('tree_left'));
        $this->assertEquals(3, $s4->getAttribute('tree_right'));
        $this->assertEquals(1, $s4->getAttribute('tree_level'));

        $s3 = Section::query()->find(3);
        $this->assertEquals(4, $s3->getAttribute('tree_left'));
        $this->assertEquals(5, $s3->getAttribute('tree_right'));
        $this->assertEquals(1, $s3->getAttribute('tree_level'));
    }

    /**
     * @depends testBasicTreeMethods
     */
    public function testFiltering()
    {
        $rootNode = Section::query()->findRoot();
        $children = Section::query()
            ->childrenOf($rootNode)
            ->orderBy('title')
            ->get();
        $this->assertContainsOnlyInstancesOf(
            Section::class,
            $children
        );
        $this->assertInstanceOf(Collection::class, $children);
        $this->assertEquals(2, $children->count());
        $this->assertEquals('Business', $children->first()->getAttribute('title'));
        $this->assertEquals('World', $children->last()->getAttribute('title'));
    }

    /**
     * @throws \Thunderwolf\EloquentNestedSet\NestedSetException
     */
    public function testScopedTreeCreation()
    {
        $thread1 = PostsThread::query()->find(1);
        $this->assertEquals('First Thread', $thread1->getAttribute('title'));
        $thread2 = PostsThread::query()->find(2);
        $this->assertEquals('Second Thread', $thread2->getAttribute('title'));
        $thread3 = PostsThread::query()->find(3);
        $this->assertEquals('Third Thread', $thread3->getAttribute('title'));

        Manager::table('posts')->truncate();

        $p1 = new Post(['body' => 'Post 1', 'code' => 'P1']);
        $p1->thread()->associate($thread1);
        $p1->makeRoot();
        $p1->save();

        $p2 = new Post(['body' => 'Post 2', 'code' => 'P2']);
        $p2->thread()->associate($thread2);
        $p2->makeRoot();
        $p2->save();

        $p3 = new Post(['body' => 'Post 3', 'code' => 'P3']);
        $p3->thread()->associate($thread3);
        $p3->makeRoot();
        $p3->save();

        $p4 = new Post(['body' => 'Post 4', 'code' => 'P4']);
        $p4->insertAsFirstChildOf($p1);
        $p4->save();

        // At the moment we must refresh manually after insert
        $p1->refresh();

        $p5 = new Post(['body' => 'Post 5', 'code' => 'P5']);
        $p5->insertAsLastChildOf($p1);
        $p5->save();

        $p6 = new Post(['body' => 'Post 6', 'code' => 'P6']);
        $p6->insertAsFirstChildOf($p2);
        $p6->save();

        $p7 = new Post(['body' => 'Post 7', 'code' => 'P7']);
        $p7->insertAsNextSiblingOf($p5);
        $p7->save();

        $p8 = new Post(['body' => 'Post 8', 'code' => 'P8']);
        $p8->insertAsFirstChildOf($p3);
        $p8->save();

        $p9 = new Post(['body' => 'Post 9', 'code' => 'P9']);
        $p9->insertAsNextSiblingOf($p8);
        $p9->save();

        $p10 = new Post(['body' => 'Post 10', 'code' => 'P10']);
        $p10->insertAsNextSiblingOf($p6);
        $p10->save();

        $p11 = new Post(['body' => 'Post 11', 'code' => 'P11']);
        $p11->insertAsNextSiblingOf($p10);
        $p11->save();

        $p12 = new Post(['body' => 'Post 12', 'code' => 'P12']);
        $p12->insertAsNextSiblingOf($p7);
        $p12->save();

        $p13 = new Post(['body' => 'Post 13', 'code' => 'P13']);
        $p13->insertAsNextSiblingOf($p12);
        $p13->save();

        $p14 = new Post(['body' => 'Post 14', 'code' => 'P14']);
        $p14->insertAsNextSiblingOf($p9);
        $p14->save();

        $p15 = new Post(['body' => 'Post 15', 'code' => 'P15']);
        $p15->insertAsNextSiblingOf($p14);
        $p15->save();

        $p16 = new Post(['body' => 'Post 16', 'code' => 'P16']);
        $p16->insertAsNextSiblingOf($p11);
        $p16->save();


        $p18 = new Post(['body' => 'Post 18', 'code' => 'P18']);
        $p18->insertAsNextSiblingOf($p16);
        $p18->save();


        $p20 = new Post(['body' => 'Post 20', 'code' => 'P20']);
        $p20->insertAsNextSiblingOf($p15);
        $p20->save();


        $p19 = new Post(['body' => 'Post 19', 'code' => 'P19']);
        $p19->insertAsNextSiblingOf($p13);
        $p19->save();

        $p17 = new Post(['body' => 'Post 17', 'code' => 'P17']);
        $p17->insertAsPrevSiblingOf($p19);
        $p17->save();

        $all = Post::query()->get();
        $expected = [
            'P1' => ['tree_left' => 1, 'tree_right' => 16, 'tree_level' => 0],
            'P4' => ['tree_left' => 2, 'tree_right' => 3, 'tree_level' => 1],
            'P5' => ['tree_left' => 4, 'tree_right' => 5, 'tree_level' => 1],
            'P7' => ['tree_left' => 6, 'tree_right' => 7, 'tree_level' => 1],
            'P12' => ['tree_left' => 8, 'tree_right' => 9, 'tree_level' => 1],
            'P13' => ['tree_left' => 10, 'tree_right' => 11, 'tree_level' => 1],
            'P17' => ['tree_left' => 12, 'tree_right' => 13, 'tree_level' => 1],
            'P19' => ['tree_left' => 14, 'tree_right' => 15, 'tree_level' => 1],

            'P2' => ['tree_left' => 1, 'tree_right' => 12, 'tree_level' => 0],
            'P6' => ['tree_left' => 2, 'tree_right' => 3, 'tree_level' => 1],
            'P10' => ['tree_left' => 4, 'tree_right' => 5, 'tree_level' => 1],
            'P11' => ['tree_left' => 6, 'tree_right' => 7, 'tree_level' => 1],
            'P16' => ['tree_left' => 8, 'tree_right' => 9, 'tree_level' => 1],
            'P18' => ['tree_left' => 10, 'tree_right' => 11, 'tree_level' => 1],

            'P3' => ['tree_left' => 1, 'tree_right' => 12, 'tree_level' => 0],
            'P8' => ['tree_left' => 2, 'tree_right' => 3, 'tree_level' => 1],
            'P9' => ['tree_left' => 4, 'tree_right' => 5, 'tree_level' => 1],
            'P14' => ['tree_left' => 6, 'tree_right' => 7, 'tree_level' => 1],
            'P15' => ['tree_left' => 8, 'tree_right' => 9, 'tree_level' => 1],
            'P20' => ['tree_left' => 10, 'tree_right' => 11, 'tree_level' => 1],
        ];
        foreach ($all as $post) {
            $code =$post->getAttribute('code');
            $this->assertEquals($expected[$code]['tree_left'], $post->getLft());
            $this->assertEquals($expected[$code]['tree_right'], $post->getRgt());
            $this->assertEquals($expected[$code]['tree_level'], $post->getLvl());
        }
    }

    public function testTree()
    {
        $thread1 = PostsThread::query()->find(1);
        $this->assertEquals('First Thread', $thread1->getAttribute('title'));
        $thread2 = PostsThread::query()->find(2);
        $this->assertEquals('Second Thread', $thread2->getAttribute('title'));
        $thread3 = PostsThread::query()->find(3);
        $this->assertEquals('Third Thread', $thread3->getAttribute('title'));

        $all = Post::query()->get();
        $expected = [
            1 => ['tree_left' => 1, 'tree_right' => 16, 'tree_level' => 0],
            4 => ['tree_left' => 2, 'tree_right' => 3, 'tree_level' => 1],
            5 => ['tree_left' => 4, 'tree_right' => 5, 'tree_level' => 1],
            7 => ['tree_left' => 6, 'tree_right' => 7, 'tree_level' => 1],
            12 => ['tree_left' => 8, 'tree_right' => 9, 'tree_level' => 1],
            13 => ['tree_left' => 10, 'tree_right' => 11, 'tree_level' => 1],
            17 => ['tree_left' => 12, 'tree_right' => 13, 'tree_level' => 1],
            19 => ['tree_left' => 14, 'tree_right' => 15, 'tree_level' => 1],

            2 => ['tree_left' => 1, 'tree_right' => 12, 'tree_level' => 0],
            6 => ['tree_left' => 2, 'tree_right' => 3, 'tree_level' => 1],
            10 => ['tree_left' => 4, 'tree_right' => 5, 'tree_level' => 1],
            11 => ['tree_left' => 6, 'tree_right' => 7, 'tree_level' => 1],
            16 => ['tree_left' => 8, 'tree_right' => 9, 'tree_level' => 1],
            18 => ['tree_left' => 10, 'tree_right' => 11, 'tree_level' => 1],

            3 => ['tree_left' => 1, 'tree_right' => 12, 'tree_level' => 0],
            8 => ['tree_left' => 2, 'tree_right' => 3, 'tree_level' => 1],
            9 => ['tree_left' => 4, 'tree_right' => 5, 'tree_level' => 1],
            14 => ['tree_left' => 6, 'tree_right' => 7, 'tree_level' => 1],
            15 => ['tree_left' => 8, 'tree_right' => 9, 'tree_level' => 1],
            20 => ['tree_left' => 10, 'tree_right' => 11, 'tree_level' => 1],
        ];
        foreach ($all as $post) {
            $this->assertEquals($expected[$post->getKey()]['tree_left'], $post->getLft());
            $this->assertEquals($expected[$post->getKey()]['tree_right'], $post->getRgt());
            $this->assertEquals($expected[$post->getKey()]['tree_level'], $post->getLvl());
        }

        $firstPost = Post::query()->findRoot($thread2->getKey());  // first message of the discussion
        $this->assertEquals(2, $firstPost->getKey());

        $discussion = Post::query()->findTree($thread3->getKey()); // all messages of the discussion
        $this->assertContainsOnlyInstancesOf(
            Post::class,
            $discussion
        );
        $this->assertInstanceOf(Collection::class, $discussion);
        $this->assertEquals(6, $discussion->count());
        $expected = [
            3 => ['tree_left' => 1, 'tree_right' => 12, 'tree_level' => 0],
            8 => ['tree_left' => 2, 'tree_right' => 3, 'tree_level' => 1],
            9 => ['tree_left' => 4, 'tree_right' => 5, 'tree_level' => 1],
            14 => ['tree_left' => 6, 'tree_right' => 7, 'tree_level' => 1],
            15 => ['tree_left' => 8, 'tree_right' => 9, 'tree_level' => 1],
            20 => ['tree_left' => 10, 'tree_right' => 11, 'tree_level' => 1],
        ];
        foreach ($discussion as $post) {
            $this->assertEquals($expected[$post->getKey()]['tree_left'], $post->getLft());
            $this->assertEquals($expected[$post->getKey()]['tree_right'], $post->getRgt());
            $this->assertEquals($expected[$post->getKey()]['tree_level'], $post->getLvl());
        }
        $firstPostOfEveryDiscussion = Post::query()->findRoots();
        $this->assertContainsOnlyInstancesOf(
            Post::class,
            $firstPostOfEveryDiscussion
        );
        $this->assertInstanceOf(Collection::class, $firstPostOfEveryDiscussion);
        $this->assertEquals(3, $firstPostOfEveryDiscussion->count());
        $expected = [
            1 => ['tree_left' => 1, 'tree_right' => 16, 'tree_level' => 0],
            2 => ['tree_left' => 1, 'tree_right' => 12, 'tree_level' => 0],
            3 => ['tree_left' => 1, 'tree_right' => 12, 'tree_level' => 0],
        ];
        foreach ($firstPostOfEveryDiscussion as $post) {
            $this->assertEquals($expected[$post->getKey()]['tree_left'], $post->getLft());
            $this->assertEquals($expected[$post->getKey()]['tree_right'], $post->getRgt());
            $this->assertEquals($expected[$post->getKey()]['tree_level'], $post->getLvl());
        }

        Post::query()->inTree($thread1->getKey())->delete(); // delete an entire discussion

        $firstPostOfEveryDiscussion = Post::query()->findRoots();
        $this->assertEquals(2, $firstPostOfEveryDiscussion->count());

        $deletedPost = Post::query()->find(4);
        $this->assertEquals(null, $deletedPost);
    }
}
