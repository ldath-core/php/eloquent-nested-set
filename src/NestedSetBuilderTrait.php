<?php

namespace Thunderwolf\EloquentNestedSet;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

trait NestedSetBuilderTrait
{
    // tree filter methods
    /**
     * Filter the query to restrict the result to root objects
     *
     * @return self The current query, for fluid interface
     */
    public function treeRoots(): self
    {
        return $this->where($this->model->getLftName(), 1);
    }

    /**
     * Returns the objects in a certain tree, from the tree scope
     *
     * @param int|null $scope Scope to determine which objects node to return
     * @return self The current query, for fluid interface
     */
    public function inTree(int $scope = null): self
    {
        return $this->where($this->model->getNestedSetScopeName(), $scope);
    }

    /**
     * Filter the query to restrict the result to descendants of an object
     *
     * @param Model $node The object to use for descendant search
     * @return self The current query, for fluid interface
     */
    public function descendantsOf(Model $node): self
    {
        $query = $this;
        if ($this->model->isNestedSetScopeUsed()) {
            $query = $query->inTree($node->getNestedSetScope());
        }
        return $query
            ->where($this->model->getLftName(), '>', $node->getLft())
            ->where($this->model->getLftName(), '<', $node->getRgt());
    }

    /**
     * Filter the query to restrict the result to the branch of an object.
     * Same as descendantsOf(), except that it includes the object passed as parameter in the result
     *
     * @param Model $node The object to use for branch search
     * @return self The current query, for fluid interface
     */
    public function branchOf(Model $node): self
    {
        $query = $this;
        if ($this->model->isNestedSetScopeUsed()) {
            $query = $query->inTree($node->getNestedSetScope());
        }
        return $query
            ->where($this->model->getLftName(), '>=', $node->getLft())
            ->where($this->model->getLftName(), '<=', $node->getRgt());
    }

    /**
     * Filter the query to restrict the result to children of an object
     *
     * @param Model $node The object to use for child search
     * @return self The current query, for fluid interface
     */
    public function childrenOf(Model $node): self
    {
        return $this->descendantsOf($node)->where($this->model->getLvlName(), $node->getLvl() + 1);
    }

    /**
     * Filter the query to restrict the result to siblings of an object.
     * The result does not include the object passed as parameter.
     *
     * @param Model $node The object to use for sibling search
     * @return self The current query, for fluid interface
     */
    public function siblingsOf(Model $node): self
    {
        if ($node->isRoot()) {
            return $this->whereRaw('? 1<>1', [$this->model->getLvlName()]);
        } else {
            return $this->childrenOf($node->getParent())->where($node->getKeyName(), '<>', $node->getKey());
        }
    }

    /**
     * Filter the query to restrict the result to ancestors of an object
     *
     * @param Model $node The object to use for ancestors search
     * @return self The current query, for fluid interface
     */
    public function ancestorsOf(Model $node): self
    {
        $query = $this;
        if ($this->model->isNestedSetScopeUsed()) {
            $query = $query->inTree($node->getNestedSetScope());
        }
        return $query
            ->where($this->model->getLftName(), '<', $node->getLft())
            ->where($this->model->getRgtName(), '>', $node->getRgt());
    }

    /**
     * Filter the query to restrict the result to roots of an object.
     * Same as ancestorsOf(), except that it includes the object passed as parameter in the result
     *
     * @param Model $node The object to use for roots search
     * @return self The current query, for fluid interface
     */
    public function rootsOf(Model $node): self
    {
        $query = $this;
        if ($this->model->isNestedSetScopeUsed()) {
            $query = $query->inTree($node->getNestedSetScope());
        }
        return $query
            ->where($this->model->getLftName(), '<=', $node->getLft())
            ->where($this->model->getRgtName(), '>=', $node->getRgt());
    }

    // order methods

    /**
     * Order the result by branch, i.e. natural tree order
     *
     * @param bool $reverse if true, reverses the order
     * @return self The current query, for fluid interface
     */
    public function orderByBranch(bool $reverse = false): self
    {
        if ($reverse) {
            return $this->orderByDesc($this->model->getLftName());
        } else {
            return $this->orderBy($this->model->getLftName());
        }
    }

    /**
     * Order the result by level, the closer to the root first
     *
     * @param bool $reverse if true, reverses the order
     * @return self The current query, for fluid interface
     */
    public function orderByLevel(bool $reverse = false): self
    {
        if ($reverse) {
            return $this->orderBy($this->model->getRgtName());
        } else {
            return $this->orderByDesc($this->model->getRgtName());
        }
    }

    // termination methods

    /**
     * Returns the root node for the tree
     *
     * @param  int|null $scope Scope to determine which root node to return
     * @return Model|object|self|null
     */
    public function findRoot(int $scope = null)
    {
        $query = $this->where($this->model->getLftName(), 1);
        if ($this->model->isNestedSetScopeUsed()) {
            $query = $query->inTree($scope);
        }
        return $query->first();
    }

    /**
     * Returns the root objects for all trees.
     *
     * @return Builder[]|Collection the list of results, formatted by the current formatter
     */
    public function findRoots()
    {
        return $this
            ->treeRoots()
            ->get();
    }

    /**
     * Returns the tree of objects
     *
     * @param int|null $scope Scope to determine which objects node to return
     * @return Builder[]|Collection
     */
    public function findTree(int $scope = null)
    {
        $query = $this;
        if ($this->model->isNestedSetScopeUsed()) {
            $query = $query->inTree($scope);
        }
        return $query
            ->orderByBranch()
            ->get();
    }

    /**
     * Delete an entire tree
     *
     * @param int $scope Scope to determine which tree to delete
     * @return int The number of deleted nodes
     */
    public function deleteTree($scope = null, ConnectionInterface $con = null): int
    {
        $query = $this;
        if ($this->model->isNestedSetScopeUsed()) {
            $query = $query->inTree($scope);
        }
        return $query->delete();
    }

    /**
     * Update the tree to allow insertion of a leaf at the specified position
     *
     * @param int $scope scope column value
     */
    public function fixLevels(int $scope)
    {
        $dataFetcher = $this->findTree($scope);

        // set the class once to avoid overhead in the loop
        $cls = get_class($this->model);
        $level = null;
        // iterate over the statement
        foreach ($dataFetcher as $row) {
            $obj = new $cls($row->toArray());

            // compute level
            // Algorithm shamelessly stolen from sfPropelActAsNestedSetBehaviorPlugin
            // Probably authored by Tristan Rivoallan
            if ($level === null) {
                $level = 0;
                $i = 0;
                $prev = array($obj->getRgt());
            } else {
                while ($obj->getRgt() > $prev[$i]) {
                    $i--;
                }
                $level = ++$i;
                $prev[$i] = $obj->getRgt();
            }

            // update level in node if necessary
            if ($obj->getLvl() !== $level) {
                $obj->setLvl($level);
                $obj->save();
            }
        }
    }
}