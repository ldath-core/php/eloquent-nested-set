<?php

namespace Thunderwolf\EloquentNestedSet;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
//use Illuminate\Support\Facades\DB;
//use Illuminate\Database\Capsule\Manager as DB;

trait NestedSet
{
    /**
     * Generated Model configuration cache
     *
     * @var array
     */
    public array $nestedSetConfiguration = [];

    /**
     * Keep track of the number of performed operations.
     *
     * @var int
     */
    public static int $actionsPerformed = 0;

    /**
     * Internal cache for children nodes
     *
     * @var Collection|null
     */
    protected ?Collection $collNestedSetChildren = null;

    /**
     * Internal cache for parent node
     *
     * @var Model|null
     */
    protected ?Model $aNestedSetParent = null;

    /**
     * Queries to be executed in the save transaction
     *
     * @var        array
     */
    protected array $nestedSetQueries = [];

    /**
     * Configuration for the NestedSet Trait
     *
     * @return array Array in the format NestedSetHelper::COLUMNS
     */
    abstract public static function nestedSet(): array;

    /**
     * Override -> Create a new Eloquent query builder for the model.
     * If you have more Behaviors using this kind on Override create own and use Trait NestedSetBuilderTrait
     *
     * @param  Builder  $query
     * @return NestedSetBuilder
     */
    public function newEloquentBuilder($query): NestedSetBuilder
    {
        return new NestedSetBuilder($query);
    }

    /**
     * Booting Eloquent Sluggable Trait
     *
     *  creating and created: sent before and after records have been created.
     *  updating and updated: sent before and after records are updated.
     *  saving and saved: sent before and after records are saved (i.e created or updated).
     *  deleting and deleted: sent before and after records are deleted or soft-deleted.
     *  restoring and restored: sent before and after soft-deleted records are restored.
     *  retrieved: sent after records have been retrieved.
     *
     * @return void
     * @throws NestedSetException
     */
    public static function bootNestedSet()
    {
        static::saving(function ($model) {
            // nested_set behavior
            if (!$model->exists() && $model->isRoot()) {
                // check if no other root exist in, the tree
                $query = get_called_class()::where($model->getLftName(), 1);
                if ($model->isNestedSetScopeUsed()) {
                    $query = $query->where($model->getNestedSetScopeName(), $model->getNestedSetScope());
                }
                $nbRoots = $query->count();
                if ($nbRoots > 0) {
                    throw new NestedSetException('A root node already exists in this tree. To allow multiple root nodes, add the `use_scope` parameter in the nested_set behavior tag.');
                }
            }
            $model->processNestedSetQueries();
        });

        static::deleting(function ($model) {
            // nested_set behavior
            if ($model->isRoot()) {
                throw new NestedSetException('Deletion of a root node is disabled for nested sets. Use twMediaFolderPeer::deleteTree() instead to delete an entire tree');
            }
            $model->clearAllReferences();
            if ($model->isInTree()) {
                $model->deleteDescendants();
            }
        });

        static::deleted(function ($model) {
            // nested_set behavior
            if ($model->isInTree()) {
                // fill up the room that was used by the node
                self::shiftRLValues(get_class($model), -2, $model->getRgt() + 1, null, $model->getNestedSetScope());
            }
        });
    }

    /**
     * Process configuration only once per object
     *
     * @return array
     * @throws NestedSetException
     */
    public function getNestedSetConfiguration(): array
    {
        if (empty($this->nestedSetConfiguration)) {
            $this->nestedSetConfiguration = NestedSetHelper::parseColumns(get_called_class()::nestedSet());
        }
        return $this->nestedSetConfiguration;
    }


    /**
     * Get the [use_scope] configuration value.
     *
     * @return bool
     * @throws NestedSetException
     */
    public function isNestedSetScopeUsed(): bool
    {
        return !!$this->getNestedSetConfiguration()['use_scope'];
    }

    /**
     * Get the [scope_column] column key name.
     *
     * @return string
     * @throws NestedSetException
     */
    public function getNestedSetScopeName(): string
    {
        return $this->getNestedSetConfiguration()['scope_column'];
    }

    /**
     * Get the [scope_column] column value.
     *
     * @return int|null
     * @throws NestedSetException
     */
    public function getNestedSetScope(): ?int
    {
        if (!$this->isNestedSetScopeUsed()) {
            return null;
        }
        return $this->getAttributeValue($this->getNestedSetScopeName());
    }

    /**
     * Set the [scope_column] column value.
     *
     * @param int $rank
     * @return void
     * @throws NestedSetException
     */
    public function setNestedSetScope(int $rank): void
    {
        $this->setAttribute($this->getNestedSetScopeName(), $rank);
    }

    // nested_set behavior

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     */
    public function clearAllReferences()
    {
        // nested_set behavior
        $this->clearNestedSetChildren();
        $this->aNestedSetParent = null;
    }

    /**
     * Execute queries that were saved to be run inside the save transaction
     */
    protected function processNestedSetQueries()
    {
        foreach ($this->nestedSetQueries as $query) {
            call_user_func_array($query['callable'], $query['arguments']);
        }
        $this->nestedSetQueries = array();
    }

    // accessing methods

    /**
     * Get the lft key name.
     *
     * @return string
     * @throws NestedSetException
     */
    public function getLftName(): string
    {
        return $this->getNestedSetConfiguration()['left'];;
    }

    /**
     * Get the rgt key name.
     *
     * @return string
     * @throws NestedSetException
     */
    public function getRgtName(): string
    {
        return $this->getNestedSetConfiguration()['right'];
    }

    /**
     * Get the lvl key name.
     *
     * @return string
     * @throws NestedSetException
     */
    public function getLvlName(): string
    {
        return $this->getNestedSetConfiguration()['level'];
    }

    /**
     * Get the value of the model's lft key.
     *
     * @return int|null
     * @throws NestedSetException
     */
    public function getLft(): ?int
    {
        return $this->getAttributeValue($this->getLftName());
    }

    /**
     * Set the value of the model's lft key.
     *
     * @param int $value
     * @return mixed
     * @throws NestedSetException
     */
    public function setLft(int $value)
    {
        return $this->setAttribute($this->getLftName(), $value);
    }

    /**
     * Get the value of the model's rgt key.
     *
     * @return int|null
     * @throws NestedSetException
     */
    public function getRgt(): ?int
    {
        return $this->getAttributeValue($this->getRgtName());
    }

    /**
     * Set the value of the model's rgt key.
     *
     * @param int $value
     * @return mixed
     * @throws NestedSetException
     */
    public function setRgt(int $value)
    {
        return $this->setAttribute($this->getRgtName(), $value);
    }

    /**
     * Get the value of the model's lvl key
     *
     * @return int|null
     * @throws NestedSetException
     */
    public function getLvl(): ?int
    {
        return $this->getAttributeValue($this->getLvlName());
    }

    /**
     * Set the value of the model's lvl key
     *
     * @param int $value
     * @return mixed
     * @throws NestedSetException
     */
    public function setLvl(int $value)
    {
        return $this->setAttribute($this->getLvlName(), $value);
    }

    /**
     * Creates the supplied node as the root node.
     *
     * @return $this
     * @throws NestedSetException
     */
    public function makeRoot(): self
    {
        if ($this->getLft() || $this->getRgt()) {
            throw new NestedSetException('Cannot turn an existing node into a root node.');
        }

        $this->setLft(1);
        $this->setRgt(2);
        $this->setLvl(0);

        return $this;
    }

    // inspection methods

    /**
     * Tests if object is a node, i.e. if it is inserted in the tree
     *
     * @return bool
     * @throws NestedSetException
     */
    public function isInTree(): bool
    {
        return $this->getLft() > 0 && $this->getRgt() > $this->getLft();
    }

    /**
     * Get whether node is root.
     *
     * @return bool
     * @throws NestedSetException
     */
    public function isRoot(): bool
    {
        return $this->isInTree() && $this->getLft() == 1;
    }

    /**
     * Tests if node is a leaf
     *
     * @return bool
     * @throws NestedSetException
     */
    public function isLeaf(): bool
    {
        return $this->isInTree() && ($this->getRgt() - $this->getLft()) == 1;
    }

    /**
     * Tests if node is a descendant of another node
     *
     * @param NestedSet $parent
     * @return bool
     * @throws NestedSetException
     */
    public function isDescendantOf(self $parent): bool
    {
        if ($this->isNestedSetScopeUsed()) {
            if ($this->getNestedSetScope() !== $parent->getNestedSetScope()) {
                return false; //since the `this` and $parent are in different scopes, there's no way that `this` is a descendant of $parent.
            }
        }

        return $this->isInTree() && $this->getLft() > $parent->getLft() && $this->getRgt() < $parent->getRgt();
    }

    /**
     * Tests if node is an ancestor of another node
     *
     * @param self $child
     * @return bool
     * @throws NestedSetException
     */
    public function isAncestorOf(self $child): bool
    {
        return $child->isDescendantOf($this);
    }

    /**
     * Tests if object has an ancestor
     *
     * @return bool
     * @throws NestedSetException
     */
    public function hasParent(): bool
    {
        return $this->getLvl() > 0;
    }

    /**
     * Sets the cache for parent node of the current object.
     * Warning: this does not move the current object in the tree.
     * Use moveToFirstChildOf() or moveToLastChildOf() for that purpose
     *
     * @param NestedSet|null $parent
     * @return $this|null The current object, for fluid interface
     */
    protected function setParent(?self $parent = null): ?self
    {
        $this->aNestedSetParent = $parent;

        return $this;
    }

    /**
     * Gets parent node for the current object if it exists
     * The result is cached so further calls to the same method don't issue any queries
     */
    public function getParent(): ?Model
    {
        if ($this->aNestedSetParent === null && $this->hasParent()) {
            $this->aNestedSetParent = get_class($this)::query()->ancestorsOf($this)->orderByLevel(true)->first();
        }

        return $this->aNestedSetParent;

    }

    /**
     * Determines if the node has previous sibling
     *
     * @return bool
     * @throws NestedSetException
     */
    public function hasPrevSibling(): bool
    {
        if (!(self::isValid($this))) {
            return false;
        }

        $query = get_class($this)::query()->where($this->getRgtName(), $this->getLft() - 1);
        if ($this->isNestedSetScopeUsed()) {
            $query = $query->inTree($this->getNestedSetScope());
        }
        return $query->count() > 0;
    }

    /**
     * Gets previous sibling for the given node if it exists
     *
     * @return Model|null
     * @throws NestedSetException
     */
    public function getPrevSibling(): ?Model
    {
        $query = get_class($this)::query()->where($this->getRgtName(), $this->getLft() - 1);
        if ($this->isNestedSetScopeUsed()) {
            $query = $query->inTree($this->getNestedSetScope());
        }
        return $query->first();
    }

    /**
     * Determines if the node has next sibling
     *
     * @return bool
     * @throws NestedSetException
     */
    public function hasNextSibling(): bool
    {
        if (!(self::isValid($this))) {
            return false;
        }

        $query = get_class($this)::query()->where($this->getLftName(), $this->getRgt() + 1);
        if ($this->isNestedSetScopeUsed()) {
            $query = $query->inTree($this->getNestedSetScope());
        }
        return $query->count() > 0;
    }

    /**
     * Gets next sibling for the given node if it exists
     *
     * @return Model|null
     * @throws NestedSetException
     */
    public function getNextSibling(): ?Model
    {
        $query = get_class($this)::query()->where($this->getLftName(), $this->getRgt() + 1);
        if ($this->isNestedSetScopeUsed()) {
            $query = $query->inTree($this->getNestedSetScope());
        }
        return $query->first();
    }

    /**
     * Clears out the $collNestedSetChildren collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be re-fetched by subsequent calls to accessor method.
     *
     * @return void
     */
    protected function clearNestedSetChildren()
    {
        $this->collNestedSetChildren = null;
    }

    /**
     * Initializes the $collNestedSetChildren collection.
     *
     * @return void
     */
    protected function initNestedSetChildren()
    {
        $this->collNestedSetChildren = new Collection();
    }

    /**
     * Adds an element to the internal $collNestedSetChildren collection.
     * Beware that this doesn't insert a node in the tree.
     * This method is only used to facilitate children hydration.
     *
     * @param self $node
     * @return void
     */
    protected function addNestedSetChild(self $node)
    {
        if ($this->collNestedSetChildren === null) {
            $this->initNestedSetChildren();
        }

        if (is_null($this->collNestedSetChildren)) {
            $this->collNestedSetChildren = new Collection();
        }

        if (!$this->collNestedSetChildren->contains($node->getKey())) {
            // only add it if the **same** object is not already associated
            $this->collNestedSetChildren->merge([$node]);
            $node->setParent($this);
        }
    }

    /**
     * Tests if node has children
     *
     * @return bool
     * @throws NestedSetException
     */
    public function hasChildren(): bool
    {
        return ($this->getRgt() - $this->getLft()) > 1;
    }

    /**
     * Gets the children of the given node
     *
     * @return Collection|null
     * @throws NestedSetException
     */
    public function getChildren(): ?Collection
    {
        if (null === $this->collNestedSetChildren) {
            if ($this->isLeaf() || (!$this->exists() && null === $this->collNestedSetChildren)) {
                // return empty collection
                $this->initNestedSetChildren();
            } else {
                $this->collNestedSetChildren = get_class($this)::query()->childrenOf($this)->orderByBranch()->get();
            }
        }

        return $this->collNestedSetChildren;
    }

    /**
     * Gets number of children for the given node
     *
     * @return int Number of children
     * @throws NestedSetException
     */
    public function countChildren(): int
    {
        if (null === $this->collNestedSetChildren) {
            if ($this->isLeaf() || (!$this->exists() && null === $this->collNestedSetChildren)) {
                return 0;
            } else {
                return get_class($this)::query()->childrenOf($this)->count();
            }
        } else {
            return count($this->collNestedSetChildren);
        }
    }

    /**
     * Gets the first child of the given node
     *
     * @return Model|null
     * @throws NestedSetException
     */
    public function getFirstChild(): ?Model
    {
        if ($this->isLeaf()) {
            return null;
        } else {
            return get_class($this)::query()->childrenOf($this)->orderByBranch()->first();
        }
    }

    /**
     * Gets the last child of the given node
     *
     * @return Model|null
     * @throws NestedSetException
     */
    public function getLastChild(): ?Model
    {
        if ($this->isLeaf()) {
            return null;
        } else {
            return get_class($this)::query()->childrenOf($this)->orderByBranch(true)->first();
        }
    }

    /**
     * Gets the siblings of the given node
     *
     * @param bool $includeCurrent
     * @return Collection|null
     * @throws NestedSetException
     */
    public function getSiblings(bool $includeCurrent = false): ?Collection
    {
        if ($this->isRoot()) {
            return null;
        } else {
            $query = get_class($this)::query()->childrenOf($this->getParent())->orderByBranch();
            if (!$includeCurrent) {
                $query = $query->where($this->getKeyName(), '<>', $this->getKey());
            }

            return $query->get();
        }
    }

    /**
     * Gets descendants for the given node
     *
     * @return Collection|null
     * @throws NestedSetException
     */
    public function getDescendants(): ?Collection
    {
        if ($this->isLeaf()) {
            return null;
        } else {
            return get_class($this)::query()->descendantsOf($this)->orderByBranch()->get();
        }
    }

    /**
     * Gets number of descendants for the given node
     *
     * @return int Number of descendants
     * @throws NestedSetException
     */
    public function countDescendants(): int
    {
        if ($this->isLeaf()) {
            return 0;
        } else {
            return get_class($this)::query()->descendantsOf($this)->count();
        }
    }

    /**
     * Gets descendants for the given node, plus the current node
     *
     * @return Collection|null
     */
    public function getBranch(): ?Collection
    {
        return get_class($this)::query()->branchOf($this)->orderByBranch()->get();
    }

    /**
     * Gets ancestors for the given node, starting with the root node
     * Use it for breadcrumb paths for instance
     *
     * @return Collection|null
     * @throws NestedSetException
     */
    public function getAncestors(): ?Collection
    {
        if ($this->isRoot()) {
            // save one query
            return null;
        } else {
            return get_class($this)::query()->ancestorsOf($this)->orderByBranch()->get();
        }
    }

    // node insertion methods (require calling save() afterwards)

    /**
     * Inserts the given $child node as first child of current
     * The modifications in the current object and the tree
     * is persisted and updated
     *
     * @param Model $child Child Node Object
     * @param string $where By default, we will put new child as the first child. In the future versions this will be changed to enum
     * @return $this Current Node Object
     * @throws NestedSetException
     */
    public function addChild(Model $child, string $where = 'first'): self
    {
        if (!$this->exists()) {
            throw new NestedSetException('A twMediaFolder object must not be new to accept children.');
        }
        switch ($where) {
            case 'first':
                $child->insertAsFirstChildOf($this)->save();
                break;
            case 'last':
                $child->insertAsLastChildOf($this)->save();
                break;
            default:
                throw new NestedSetException('unknown $where: '.$where.' when adding child');
        }

        return $this->refresh();
    }

    /**
     * Inserts the current node as first child of given $parent node
     * The modifications in the current object and the tree
     * are not persisted until the current object is saved.
     *
     * @param Model $parent
     * @return $this
     * @throws NestedSetException
     */
    public function insertAsFirstChildOf(Model $parent): self
    {
        if ($this->isInTree()) {
            throw new NestedSetException('A Model object must not already be in the tree to be inserted. Use the moveToFirstChildOf() instead.');
        }
        $left = $parent->getLft() + 1;
        // Update node properties
        return $this->updateChildNodeProperties($parent, $left);
    }

    /**
     * Inserts the current node as last child of given $parent node
     * The modifications in the current object and the tree
     * are not persisted until the current object is saved.
     *
     * @param Model $parent
     * @return $this
     * @throws NestedSetException
     */
    public function insertAsLastChildOf(Model $parent): self
    {
        if ($this->isInTree()) {
            throw new NestedSetException('A twMediaFolder object must not already be in the tree to be inserted. Use the moveToLastChildOf() instead.');
        }
        $left = $parent->getRgt();
        // Update node properties
        return $this->updateChildNodeProperties($parent, $left);
    }

    /**
     * @param Model $parent
     * @param int $left
     * @return $this
     * @throws NestedSetException
     */
    protected function updateChildNodeProperties(Model $parent, int $left): self
    {
        $this->setLft($left);
        $this->setRgt($left + 1);
        $this->setLvl($parent->getLvl() + 1);
        $scope = $parent->getNestedSetScope();
        if ($this->isNestedSetScopeUsed()) {
            $this->setNestedSetScope($scope);
        }
        // update the children collection of the parent
        $parent->addNestedSetChild($this);

        // Keep the tree modification query for the save() transaction
        $this->nestedSetQueries[] = array(
            'callable' => array(get_class($this), 'makeRoomForLeaf'),
            'arguments' => array(get_class($this), $left, $scope, !$this->exists() ? null : $this)
        );

        return $this;
    }

    /**
     * Inserts the current node as prev sibling given $sibling node
     * The modifications in the current object and the tree
     * are not persisted until the current object is saved.
     *
     * @param Model $sibling
     * @return $this
     * @throws NestedSetException
     */
    public function insertAsPrevSiblingOf(Model $sibling): self
    {
        if ($this->isInTree()) {
            throw new NestedSetException('A twMediaFolder object must not already be in the tree to be inserted. Use the moveToPrevSiblingOf() instead.');
        }
        $left = $sibling->getLft();
        // Update node properties
        return $this->updateSiblingNodeProperties($sibling, $left);
    }

    /**
     * Inserts the current node as next sibling given $sibling node
     * The modifications in the current object and the tree
     * are not persisted until the current object is saved.
     *
     * @param Model $sibling
     * @return $this
     * @throws NestedSetException
     */
    public function insertAsNextSiblingOf(Model $sibling): self
    {
        if ($this->isInTree()) {
            throw new NestedSetException('A twMediaFolder object must not already be in the tree to be inserted. Use the moveToNextSiblingOf() instead.');
        }
        $left = $sibling->getRgt() + 1;
        // Update node properties
        return $this->updateSiblingNodeProperties($sibling, $left);
    }

    /**
     * @param Model $sibling
     * @param int $left
     * @return $this
     * @throws NestedSetException
     */
    protected function updateSiblingNodeProperties(Model $sibling, int $left): self
    {
        $this->setLft($left);
        $this->setRgt($left + 1);
        $this->setLvl($sibling->getLvl());
        $scope = $sibling->getNestedSetScope();
        if ($this->isNestedSetScopeUsed()) {
            $this->setNestedSetScope($scope);
        }
        // Keep the tree modification query for the save() transaction
        $this->nestedSetQueries[] = array(
            'callable' => array(get_class($this), 'makeRoomForLeaf'),
            'arguments' => array(get_class($this), $left, $scope, !$this->exists() ? null : $this)
        );

        return $this;
    }

    // node move methods (immediate, no need to save() afterwards)

    /**
     * Moves current node and its subtree to be the first child of $parent
     * The modifications in the current object and the tree are immediate
     *
     * @param Model $parent Parent Node
     * @return $this The current Node
     * @throws NestedSetException
     */
    public function moveToFirstChildOf(Model $parent): self
    {
        if (!$this->isInTree()) {
            throw new NestedSetException('A twMediaFolder object must be already in the tree to be moved. Use the insertAsFirstChildOf() instead.');
        }
        if ($parent->isDescendantOf($this)) {
            throw new NestedSetException('Cannot move a node as child of one of its subtree nodes.');
        }

        return $this->moveSubtreeTo($parent->getLft() + 1, $parent->getLvl() - $this->getLvl() + 1, $parent->getNestedSetScope());
    }

    /**
     * Moves current node and its subtree to be the last child of $parent
     * The modifications in the current object and the tree are immediate
     *
     * @param Model $parent Parent node
     * @return $this Current node
     * @throws NestedSetException
     */
    public function moveToLastChildOf(Model $parent): self
    {
        if (!$this->isInTree()) {
            throw new NestedSetException('A twMediaFolder object must be already in the tree to be moved. Use the insertAsLastChildOf() instead.');
        }
        if ($parent->isDescendantOf($this)) {
            throw new NestedSetException('Cannot move a node as child of one of its subtree nodes.');
        }

        return $this->moveSubtreeTo($parent->getRgt(), $parent->getLvl() - $this->getLvl() + 1, $parent->getNestedSetScope());
    }

    /**
     * Moves current node and its subtree to be the previous sibling of $sibling
     * The modifications in the current object and the tree are immediate
     *
     * @param Model $sibling Sibling node
     * @return $this The current node
     * @throws NestedSetException
     */
    public function moveToPrevSiblingOf(Model $sibling): self
    {
        if (!$this->isInTree()) {
            throw new NestedSetException('A twMediaFolder object must be already in the tree to be moved. Use the insertAsPrevSiblingOf() instead.');
        }
        if ($sibling->isRoot()) {
            throw new NestedSetException('Cannot move to previous sibling of a root node.');
        }
        if ($sibling->isDescendantOf($this)) {
            throw new NestedSetException('Cannot move a node as sibling of one of its subtree nodes.');
        }

        return $this->moveSubtreeTo($sibling->getLft(), $sibling->getLvl() - $this->getLvl(), $sibling->getNestedSetScope());
    }

    /**
     * Moves current node and its subtree to be the next sibling of $sibling
     * The modifications in the current object and the tree are immediate
     *
     * @param Model $sibling Sibling node
     * @return $this The current node
     * @throws NestedSetException
     */
    public function moveToNextSiblingOf(Model $sibling): self
    {
        if (!$this->isInTree()) {
            throw new NestedSetException('A twMediaFolder object must be already in the tree to be moved. Use the insertAsNextSiblingOf() instead.');
        }
        if ($sibling->isRoot()) {
            throw new NestedSetException('Cannot move to next sibling of a root node.');
        }
        if ($sibling->isDescendantOf($this)) {
            throw new NestedSetException('Cannot move a node as sibling of one of its subtree nodes.');
        }

        return $this->moveSubtreeTo($sibling->getRgt() + 1, $sibling->getLvl() - $this->getLvl(), $sibling->getNestedSetScope());
    }

    /**
     * Move current node and its children to location $destLeft and updates rest of tree
     *
     * @param int $destLeft Destination left value
     * @param int $levelDelta Delta to add to the levels
     * @param int|null $targetScope
     * @throws NestedSetException
     */
    protected function moveSubtreeTo(int $destLeft, int $levelDelta, int $targetScope = null): self
    {
        $preventDefault = false;
        $left  = $this->getLft();
        $right = $this->getRgt();
        $scope = $this->getNestedSetScope();

        if ($targetScope === null) {
            $targetScope = $scope;
        }

        $treeSize = $right - $left +1;

        get_class($this)::getConnection()->beginTransaction();
        try {
            // make room next to the target for the subtree
            self::shiftRLValues(get_class($this), $treeSize, $destLeft, null, $targetScope);

            if ($targetScope != $scope) {
                //move subtree to < 0, so the items are out of scope.
                self::shiftRLValues(get_class($this), -$right, $left, $right, $scope);

                //update scopes
                self::setNegativeScope(get_class($this), $targetScope);

                //update levels
                self::shiftLevel(get_class($this), $levelDelta, $left - $right, 0, $targetScope);

                //move the subtree to the target
                self::shiftRLValues(get_class($this),($right - $left) + $destLeft, $left - $right, 0, $targetScope);

                $preventDefault = true;
            }

            if (!$preventDefault) {
                if ($left >= $destLeft) { // src was shifted too?
                    $left += $treeSize;
                    $right += $treeSize;
                }

                if ($levelDelta) {
                    // update the levels of the subtree
                    self::shiftLevel(get_class($this), $levelDelta, $left, $right, $scope);
                }

                // move the subtree to the target
                self::shiftRLValues(get_class($this), $destLeft - $left, $left, $right, $scope);
            }

            // remove the empty room at the previous location of the subtree
            self::shiftRLValues(get_class($this), -$treeSize, $right + 1, null, $scope);

            // update all loaded nodes
            self::updateLoadedNodes();

            get_class($this)::getConnection()->commit();

            // Return refreshed object
            $this->clearAllReferences();
            return get_class($this)::query()->find($this->getKey());
        } catch (Exception $e) {
            get_class($this)::getConnection()->rollBack();
            throw $e;
        }
    }

    // deletion methods

    /**
     * Deletes all descendants for the given node
     * Instance pooling is wiped out by this command,
     * so existing twMediaFolder instances are probably invalid (except for the current one)
     *
     * @return int number of deleted nodes
     * @throws NestedSetException
     */
    public function deleteDescendants(): int
    {
        if ($this->isLeaf()) {
            // save one query
            return 0;
        }

        $left = $this->getLft();
        $right = $this->getRgt();
        $scope = $this->getNestedSetScope();

        get_class($this)::getConnection()->beginTransaction();
        try {
            // delete descendant nodes (will empty the instance pool)
            $ret = get_class($this)::query()->descendantsOf($this)->delete();

            // fill up the room that was used by descendants
            self::shiftRLValues(get_class($this), $left - $right + 1, $right, null, $scope);

            // fix the right value for the current node, which is now a leaf
            $this->setRgt($left + 1);

            get_class($this)::getConnection()->commit();
        } catch (Exception $e) {
            get_class($this)::getConnection()->rollBack();
            throw $e;
        }

        return $ret;
    }

    /**
     * Update the tree to allow insertion of a leaf at the specified position
     *
     * @param string $origin_class
     * @param int $left left column value
     * @param int|null $scope Scope to use for the shift
     * @param mixed $prune Object to prune from the shift
     * @throws NestedSetException
     */
    public static function makeRoomForLeaf(string $origin_class, int $left, int $scope = null, $prune = null)
    {
        // Update database nodes
        self::shiftRLValues($origin_class, 2, $left, null, $scope);
    }

    /**
     * Adds $delta to all L and R values that are >= $first and <= $last.
     * '$delta' can also be negative.
     *
     * @param string $origin_class
     * @param int $delta
     * @param int $first
     * @param int|null $last
     * @param int|null $scope Scope to use for the shift
     * @return void
     * @throws NestedSetException
     */
    public static function shiftRLValues(string $origin_class, int $delta, int $first, ?int $last = null, int $scope = null)
    {
        $use_scope = NestedSetHelper::parseColumns($origin_class::nestedSet())['use_scope'];
        if ($use_scope) {
            $scope_column = NestedSetHelper::parseColumns($origin_class::nestedSet())['scope_column'];
        }

        // Shift left column values
        $left_column = NestedSetHelper::parseColumns($origin_class::nestedSet())['left'];
        $b = $origin_class::query()->where($left_column, '>=', $first);
        if (null !== $last) {
            $b = $b->where($left_column, '<=', $last);
        }
        if ($use_scope) {
            if (is_null($scope)) {
                throw new NestedSetException('Scope usage active but null scope given');
            }
            $b = $b->where($scope_column, $scope);
        }
        $b->increment($left_column, $delta);

        // Shift right column values
        $right_column = NestedSetHelper::parseColumns($origin_class::nestedSet())['right'];
        $b = $origin_class::query()->where($right_column, '>=', $first);
        if (null !== $last) {
            $b = $b->where($right_column, '<=', $last);
        }
        if ($use_scope) {
            if (is_null($scope)) {
                throw new NestedSetException('Scope usage active but null scope given');
            }
            $b = $b->where($scope_column, $scope);
        }
        $b->increment($right_column, $delta);
    }

    /**
     * Adds $delta to level for nodes having left value >= $first and right value <= $last.
     * '$delta' can also be negative.
     *
     * @param string $origin_class
     * @param int $delta Value to be shifted by, can be negative
     * @param int $first First node to be shifted
     * @param int $last Last node to be shifted
     * @param int|null $scope Scope to use for the shift
     * @return mixed
     * @throws NestedSetException
     */
    public static function shiftLevel(string $origin_class, int $delta, int $first, int $last, int $scope = null)
    {
        $use_scope = NestedSetHelper::parseColumns($origin_class::nestedSet())['use_scope'];
        if ($use_scope) {
            $scope_column = NestedSetHelper::parseColumns($origin_class::nestedSet())['scope_column'];
        }

        $left_column = NestedSetHelper::parseColumns($origin_class::nestedSet())['left'];
        $right_column = NestedSetHelper::parseColumns($origin_class::nestedSet())['right'];
        $level_column = NestedSetHelper::parseColumns($origin_class::nestedSet())['level'];

        $that = $origin_class::query()
            ->where($left_column, '>=', $first)
            ->where($right_column, '<=', $last);
        if ($use_scope) {
            if (is_null($scope)) {
                throw new NestedSetException('Scope usage active but null scope given');
            }
            $that = $that->where($scope_column, $scope);
        }

        return $that->increment($level_column, $delta);
    }

    /**
     * Tests if node is valid
     *
     * @param Model|null $node
     * @return bool
     */
    public static function isValid(Model $node = null): bool
    {
        if ($node->getRgt() > $node->getLft()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @return Model
     * @throws NestedSetException
     */
    public function reload(): Model
    {
        if (!$this->exists() || is_null($this->getKey())) {
            throw new NestedSetException("Cannot reload an unsaved object.");
        }
        return get_class($this)::query()->find($this->getKey());
    }

    /**
     * Updates all scope values for items that has negative left (<=0) values.
     *
     * @param string $origin_class
     * @param int $scope
     * @return void
     * @throws NestedSetException
     */
    public static function setNegativeScope(string $origin_class, int $scope)
    {
        $left_column = NestedSetHelper::parseColumns($origin_class::nestedSet())['left'];
        $scope_column = NestedSetHelper::parseColumns($origin_class::nestedSet())['scope_column'];
        //adjust scope value to $scope
        $origin_class::query()->where($left_column, '<=' ,0)->update([$scope_column, $scope]);
    }

    /**
     * Reload all already loaded nodes to sync them with updated db
     *
     * TODO: hard to say how to do this here???
     *
     * @return void
     */
    public static function updateLoadedNodes()
    {
        ;
    }
}