<?php

namespace Thunderwolf\EloquentNestedSet;

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\ServiceProvider;

class NestedSetServiceProvider extends ServiceProvider
{
    public function register()
    {
        Blueprint::macro('createNestedSet', function (array $columns) {
            NestedSetBlueprint::createNestedSetColumns($this, $columns);
        });

        Blueprint::macro('dropNestedSet', function (array $columns) {
            NestedSetBlueprint::dropNestedSetColumns($this, $columns);
        });
    }
}