<?php

namespace Thunderwolf\EloquentNestedSet;

class NestedSetHelper
{
    /**
     * The name of default lft column.
     */
    const LFT = 'tree_left';

    /**
     * The name of default rgt column.
     */
    const RGT = 'tree_right';

    /**
     * The name of default lvl column.
     */
    const LVL = 'tree_level';

    /**
     * Parsing columns given to
     *
     * @param array $columns
     * @return array
     * @throws NestedSetException
     */
    public static function parseColumns(array $columns): array
    {
        if (!array_key_exists('left', $columns)) {
            $columns['left'] = self::LFT;
        }
        if (!array_key_exists('right', $columns)) {
            $columns['right'] = self::RGT;
        }
        if (!array_key_exists('level', $columns)) {
            $columns['level'] = self::LVL;
        }
        if (array_key_exists('use_scope', $columns) && $columns['use_scope']) {
            if (!array_key_exists('scope_column', $columns)) {
                throw new NestedSetException('You can\'t enable scope without setting scope_column');
            };
        } else {
            $columns['use_scope'] = false;
        }
        return $columns;
    }

}