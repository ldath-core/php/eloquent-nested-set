<?php

namespace Thunderwolf\EloquentNestedSet;

use Illuminate\Database\Schema\Blueprint;

class NestedSetBlueprint
{
    /**
     * Add sluggable column to the table. Also create an index.
     *
     * @param Blueprint $table
     * @param array $columns
     */
    public static function createNestedSetColumns(Blueprint $table, array $columns)
    {
        $columns = NestedSetHelper::parseColumns($columns);
        $table->unsignedInteger($columns['left'])->default(0);
        $table->unsignedInteger($columns['right'])->default(0);
        $table->unsignedInteger($columns['level'])->default(0);
        $table->index([$columns['left'], $columns['right']]);
    }

    /**
     * Drop sluggable column and index.
     *
     * @param Blueprint $table
     * @param array $columns
     */
    public static function dropNestedSetColumns(Blueprint $table, array $columns)
    {
        $columns = NestedSetHelper::parseColumns($columns);
        $table->dropIndex([$columns['left'], $columns['right']]);
        $table->dropColumn($columns['left']);
        $table->dropColumn($columns['right']);
        $table->dropColumn($columns['level']);
    }
}