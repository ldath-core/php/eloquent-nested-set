<?php

namespace Thunderwolf\EloquentNestedSet;

use Illuminate\Database\Eloquent\Builder;

class NestedSetBuilder extends Builder
{
    use NestedSetBuilderTrait;
}